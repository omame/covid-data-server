module gitlab.com/omame/covid-data-server

go 1.16

require (
	github.com/prometheus/common v0.15.0
	github.com/sirupsen/logrus v1.7.0
)
