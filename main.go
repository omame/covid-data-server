package main

import (
	"encoding/csv"
	"io/ioutil"
	"net/http"

	"github.com/prometheus/common/log"
	"github.com/sirupsen/logrus"
)

const (
	githubBaseURL = "https://raw.githubusercontent.com/pcm-dpc/COVID-19/master"
	datiItalia    = "/dati-andamento-nazionale/dpc-covid19-ita-andamento-nazionale.csv"
	datiProvince  = "/dati-province/dpc-covid19-ita-province.csv"
	datiRegioni   = "/dati-regioni/dpc-covid19-ita-regioni.csv"
)

func main() {
	http.HandleFunc("/covid-data/italia", italiaHandler)
	http.HandleFunc("/covid-data/province", provinceHandler)
	http.HandleFunc("/covid-data/regioni", regioniHandler)
	err := http.ListenAndServe(":6666", nil)
	if err != nil {
		logrus.Fatalln(err)

	}

}

func provinceHandler(res http.ResponseWriter, req *http.Request) {
	log.Infof("Incoming request for %s", req.URL)
	data, err := loadCSV(datiProvince)
	if err != nil {
		res.WriteHeader(500)
		return
	}

	w := csv.NewWriter(res)
	err = w.Write(data[0])
	if err != nil {
		res.WriteHeader(500)
		return
	}

	for _, line := range data[1:] {
		if line[5] == "Firenze" {
			err = w.Write(line)
			if err != nil {
				res.WriteHeader(500)
				return
			}
		}
	}
	w.Flush()
}

func regioniHandler(res http.ResponseWriter, req *http.Request) {
	log.Infof("Incoming request for %s", req.URL)
	data, err := loadCSV(datiRegioni)
	if err != nil {
		res.WriteHeader(500)
		return
	}

	w := csv.NewWriter(res)
	err = w.Write(data[0])
	if err != nil {
		res.WriteHeader(500)
		return
	}

	for _, line := range data[1:] {
		if line[3] == "Toscana" {
			err = w.Write(line)
			if err != nil {
				res.WriteHeader(500)
				return
			}
		}
	}
	w.Flush()
}

func italiaHandler(res http.ResponseWriter, req *http.Request) {
	log.Infof("Incoming request for %s", req.URL)
	resp, err := http.Get(githubBaseURL + datiItalia)
	if err != nil {
		logrus.Fatalln(err)
	}
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		res.WriteHeader(500)
		return
	}
	_, err = res.Write(data)
	if err != nil {
		res.WriteHeader(500)
		return
	}
}

func loadCSV(url string) ([][]string, error) {
	resp, err := http.Get(githubBaseURL + url)
	if err != nil {
		logrus.Fatalln(err)
	}
	defer resp.Body.Close()
	r := csv.NewReader(resp.Body)
	data, err := r.ReadAll()
	if err != nil {

		return nil, err
	}
	return data, nil
}
