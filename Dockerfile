FROM alpine:3

RUN apk --no-cache add ca-certificates

COPY covid-data-server /usr/bin/covid-data-server

EXPOSE 6666
ENTRYPOINT [ "/usr/bin/covid-data-server" ]
